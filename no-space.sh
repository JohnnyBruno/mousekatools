#!/bin/bash
#This script will remove all line breaks and spaces from a string ... Please specify the filename or full path & file.

if [ -z $1 ];
then
        echo "Proper syntax is: ""no-space /path/to/filename.txt or filename.txt"""
        exit
fi
        fixedstring=$(cat $1 | sed 's/[[:space:]]//g')
        fixedstring2=$(echo $fixedstring | tr -d ' ')
        echo $fixedstring2
        exit
