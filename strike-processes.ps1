#strike a process on a bunch of computers
$computerlistpath = (Read-Host -Prompt "Pass your computer list, use the full path if not in PWD") #example "C:\Temp\test.txt" or computers.txt
$targetlist = @(cat $computerlistpath)
$badprocess = (Read-Host -Prompt "What is the name of the bad process?")

foreach ($computername in $targetlist)
{

    Get-Process -ComputerName $computername -Name $badprocess | kill

}
