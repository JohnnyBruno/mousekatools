$path (Read-Host -Prompt "Pass your computer list, use the full path if not in PWD")
$list = @(cat $path)

foreach ($i in $list)
{
    Resolve-DnsName -ErrorAction SilentlyContinue $i | Export-Csv -Append out.csv
}
