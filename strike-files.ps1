#strike files on a bunch of computers
$computerlistpath = (Read-Host -Prompt "Pass your computer list, use the full path if not in PWD") #example "C:\Temp\test.txt" or computers.txt
$targetlist = @(cat $computerlistpath)
$target = (Read-Host -Prompt "Pass the ENTIRE path and file name of our target file") #example "C:\Users\Jared\Desktop\noodes.png"
$target = $target.Replace("C:\",'') #trying to make this less confusing for the user

foreach ($computername in $targetlist) 
{

    rm -Force "\\$computername\C$\$target" 

}
