#strike WMI persistence on on a bunch of computers

$computerlistpath = (Read-Host -Prompt "Pass your computer list, use the full path if not in PWD") #example "C:\Temp\test.txt" or computers.txt
$targetlist = @(cat $computerlistpath)

echo "Both of these next things may be called the same thing ..."
$filtername = (Read-Host -Prompt "Pass the name of the WMI Filter")
$consumername = (Read-Host -Prompt "Pass the name of the WMI Consumer")

foreach ($computername in $targetlist)
{
    Invoke-Command -Computername $computername -ScriptBlock { 
    $EventFilterToCleanup = Get-WmiObject -Namespace root/subscription -Class __EventFilter -Filter "Name = '$filtername'" ;
    $EventConsumerToCleanup = Get-WmiObject -Namespace root/subscription -Class CommandLineEventConsumer -Filter "Name = '$consumername'" ;
    $FilterConsumerBindingToCleanup = Get-WmiObject -Namespace root/subscription -Query "REFERENCES OF {$($EventConsumerToCleanup.__RELPATH)} WHERE ResultClass = __FilterToConsumerBinding" ;
    $FilterConsumerBindingToCleanup | Remove-WmiObject ;
    $EventConsumerToCleanup | Remove-WmiObject ;
    $EventFilterToCleanup | Remove-WmiObject ; }


}
