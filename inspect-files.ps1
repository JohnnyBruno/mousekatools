$ErrorActionPreference='Silentlycontinue'

write-host("This script will recursively inspect files for the presense of a specified string")

#Recursively look through files for a string


    $filepath = Read-Host -Prompt 'Directory to start from (i.e. "C:\")'      #directory to look from
    $filetype = Read-Host -Prompt "What types of files do you want to inspect? (*.exe, *.txt, *.ps1) Use * for all types" #files to look at ... use * for everything
    $filestring = Read-Host -Prompt "Will look for this string in files searched" #string in files to look for


Write-host("Checking " + $filepath + " for $filetype type files containing the string $filestring") 
$files= (gci -Recurse $filepath | where Name -Like $filetype | select -Property FullName)


#Get-content for each file and search for $filestring

foreach ($i in $files){
    
    if(cat $i.FullName | select-string $filestring)
        {
        
            Write-Host($i.FullName)
        
        }
    }

